import 'package:flutter/material.dart';
import 'package:payfazz/News.dart';

class ListFavorite extends StatelessWidget{
  List<News> listfavs;

  ListFavorite(List<News> listfav){
    this.listfavs=listfav;
  }
  @override
  Widget build(BuildContext context) {
    print(listfavs.length);
    // TODO: implement build
    return Scaffold(appBar:AppBar(backgroundColor: Colors.blue,title: Text("Favorites"),centerTitle: true,),body:ListView.builder(itemBuilder:(context,index){
      return itemFav(context,index);
    },itemCount: listfavs.length,));
  }

  Widget itemFav(context,int index){
    return Container(
        margin:EdgeInsets.all(5),child: Column(children:<Widget>[SizedBox(height: 10,),Text(listfavs[index].title,style: TextStyle(fontWeight: FontWeight.bold),),SizedBox(height: 10,),Divider(),],));
  }

}