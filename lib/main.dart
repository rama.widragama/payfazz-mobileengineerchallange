import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:payfazz/News.dart';
import 'package:payfazz/ListFavorite.dart';
import 'package:http/http.dart' as http;

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Photo Streamer',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: PhotoList(),
    );
  }
}

class PhotoList extends StatefulWidget {
  @override
  PhotoListState createState() => PhotoListState();
}

class PhotoListState extends State<PhotoList> {
  StreamController<News> streamController;
  List<News> list = [];
  List<News> fav=[];

  @override
  void initState() {
    super.initState();
    streamController = StreamController.broadcast();

    streamController.stream.listen((p) => setState(() => list.add(p)));

    load(streamController);
  }

  load(StreamController<News> sc) async {



    String url = "https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty";
    var client = new http.Client();

    var req = new http.Request('get', Uri.parse(url));

    var streamedRes = await client.send(req);

    var streams=streamedRes.stream
        .transform(utf8.decoder)
        .transform(json.decoder)
        .expand((e)=>e);

    var aksi=streams.forEach((id) async {
      String url = "https://hacker-news.firebaseio.com/v0/item/" +
          id.toString() + ".json?print=pretty";
      var client = new http.Client();

      var req = new http.Request('get', Uri.parse(url));

      var streamedRes = await client.send(req);
      var stream = streamedRes.stream
          .transform(utf8.decoder)
          .transform(json.decoder);

      var action=stream.map((map)=>News.fromJson(map));
      sc.addStream(action);
      print("awdkadkakd");

  });


        //stream.map((map) => Photo.fromJsonMap(map))
        //.pipe(sc);
  }

  @override
  void dispose() {
    super.dispose();
    streamController?.close();
    streamController = null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        centerTitle: true,
        title: Text("Hacker News"),
        actions: <Widget>[IconButton(icon:Icon(Icons.favorite,color: Colors.white,),onPressed: (){
          gotListFav(context, fav);
        })],
      ),
      body: Center(
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemBuilder: (BuildContext context, int index) => _makeElement(index),
        ),
      ),
    );
  }

  bool test(News news){
   fav.map((newz){
      if(newz.id == news.id)return true;
      else false;
    });
    return fav.contains(news);
  }
  void addToFav(News news){
    if(test(news)) fav.remove(news);
    else fav.add(news);
  }

  Widget _makeElement(int index) {
    if (index >= list.length) {
      return null;
    }

    
      var date = new DateTime.fromMillisecondsSinceEpoch(list[index].tanggal * 1000);
    String formattedDate = DateFormat('MMMM-dd-yyyy').format(date);
      return Container(
        padding: EdgeInsets.all(10),
          child:
          Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        
        children: <Widget>[
          Text("#"+list[index].id.toString(),
            style: TextStyle(fontWeight: FontWeight.w200),),
          SizedBox(height: 5,),
          Text(list[index].title,style: TextStyle(fontSize: 17),)
          ,SizedBox(height: 5,)
          ,Row(mainAxisAlignment:MainAxisAlignment.spaceBetween,
            children:<Widget>[Column(
                children:<Widget>[Text(
                    "Author : "+list[index].author,
                    style: TextStyle(fontWeight:FontWeight.w200,))
                ,SizedBox(height: 5,),Text(formattedDate.toString())])
            ,IconButton(icon: test(list[index]) ? Icon(Icons.favorite ,color:  Colors.red,) : Icon(Icons.favorite_border ,color:  Colors.red,)
              ,splashColor: Colors.blue,onPressed: (){
              setState(() {
                addToFav(list[index]);
              });
              },)],),Divider()
        ],)
      );
  }
}

void gotListFav(BuildContext context,List<News> fav){
  Navigator.push(
    context,
    MaterialPageRoute(
      builder: (context) => ListFavorite(fav),
    ),
  );
}