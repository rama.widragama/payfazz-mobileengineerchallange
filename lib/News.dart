class News {
  int id;
  String title;
  String author;
  int tanggal;

  News({
    this.id,
    this.title,
    this.author,
    this.tanggal

});

factory News.fromJson(Map<String, dynamic> parsedJson){
  return News(
    id: parsedJson['id'],
    title: parsedJson['title'],
    author: parsedJson['by'],
    tanggal: parsedJson['time']
  );
}

}